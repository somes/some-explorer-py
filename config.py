import configparser

config = configparser.ConfigParser()
config.read('config.ini')

openai_api_key = config['DEFAULT']['OpenAIKey']
database_path = config['DEFAULT']['DatabasePath']
json_file_path = config['DEFAULT']['JsonFilePath']
context_template = config['DEFAULT']['ContextTemplate']

entrez_email = config['Entrez']['Email']
entrez_database = config['Entrez']['Database']
entrez_retmode = config['Entrez']['RetMode']

embedding_model = config['Embedding']['Model']
embedding_encoding = str(config['Embedding']['Encoding'])
cost_of_1k_token = float(config['Embedding']['CostOf1KToken'])
