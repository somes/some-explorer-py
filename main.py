# noinspection PyShadowingNames
def main(args):
    if args.update_db_content:
        from data import update_db_content
        update_db_content()

    if args.optimize_db:
        from data import optimize_database
        optimize_database()

    if not args.no_server:
        from app import app
        app.run(port=args.port, debug=args.debug)


if __name__ == '__main__':
    from arguments import args

    main(args)
