import json
import sqlite3
import sys
import traceback
from sqlite3 import Connection

import numpy as np
import tiktoken

from common import *
from config import *

sqlite3.register_adapter(np.ndarray, lambda arr: arr.tobytes())
sqlite3.register_converter("ARRAY", np.frombuffer)
sqlite3.register_adapter(np.float16, float)
sqlite3.register_adapter(np.float32, float)
sqlite3.register_adapter(np.float64, float)


def connect() -> Connection:
    def print_on_error(fun):
        def wrapper(*args, **kwargs):
            try:
                return fun(*args, **kwargs)
            except:
                if hasattr(fun, '__name__'):
                    fun_name = fun.__name__
                else:
                    fun_name = str(fun)
                print(f"Failed to execute `{fun_name}`:", file=sys.stderr)
                traceback.print_exc()
                raise

        return wrapper

    @print_on_error
    def token_count_of(context):
        encoding = tiktoken.get_encoding(embedding_encoding)
        tokens = encoding.encode(context)
        return len(tokens)

    @print_on_error
    def substitute_template(template: str, *args, **kwargs):
        return template.format(*args, **kwargs)

    @print_on_error
    def cosine_similarity(a, b):
        from numpy import dot
        from numpy.linalg import norm

        if isinstance(a, str):
            a = np.array(json.loads(a))
        elif isinstance(a, bytes):
            a = np.frombuffer(a)

        if isinstance(b, str):
            b = np.array(json.loads(b))
        elif isinstance(b, bytes):
            b = np.frombuffer(b)

        return dot(a, b) / (norm(a) * norm(b))

    @print_on_error
    def to_array(s):
        if isinstance(s, str):
            return np.array(json.loads(s))
        else:
            raise NotImplementedError(f"Can't create array from {type(s)}")

    # Setup SQLite DB
    conn = sqlite3.connect(database_path, isolation_level=None, detect_types=sqlite3.PARSE_DECLTYPES)
    try:
        conn.create_function('hash_context', 1, print_on_error(hash_context), deterministic=True)
        conn.create_function('token_count_of', 1, token_count_of, deterministic=True)
        conn.create_function('substitute_template', -1, substitute_template, deterministic=True)
        conn.create_function('cosine_similarity', 2, cosine_similarity, deterministic=True)
        conn.create_function('to_array', 1, to_array, deterministic=True)
        return conn
    except:
        conn.close()
        raise
