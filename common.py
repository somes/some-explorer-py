import xxhash


def hash_context(context):
    if context is None:
        return None
    else:
        return xxhash.xxh64(context).hexdigest()
