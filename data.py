import json
import math
import os.path
import re
import shutil
import sqlite3
from sqlite3 import Connection

import numpy as np
import pandas as pd
from Bio import Entrez
from openai import OpenAI
from pandas import DataFrame
from umap import UMAP as DIMENSION_REDUCTION

from config import *
from common import *
from db import connect

Entrez.email = entrez_email
openai = OpenAI(api_key=openai_api_key)


def ensure_tables(conn: Connection):
    conn.executescript('''
    CREATE TABLE IF NOT EXISTS embeddings (
        text_hash TEXT NOT NULL,
        embedding ARRAY NOT NULL,
        PRIMARY KEY (text_hash)
    );
    CREATE TABLE IF NOT EXISTS articles (
        pmid INT NOT NULL,
        abstract TEXT,
        PRIMARY KEY (pmid)
    );
    CREATE TABLE IF NOT EXISTS pca (
        template_hash TEXT NOT NULL,
        some TEXT NOT NULL,
        pc_0 REAL NOT NULL,
        pc_1 REAL NOT NULL,
        PRIMARY KEY (template_hash, some)
    );
    ''')


def load_somes(json_file_path, conn: Connection):
    table_name = 'somes'

    def dtype_to_sql_type(dtype):
        from pandas.api.types import is_integer_dtype, is_numeric_dtype, is_any_real_numeric_dtype, is_string_dtype

        if is_integer_dtype(dtype):
            return 'INT'
        elif is_any_real_numeric_dtype(dtype):
            return 'REAL'
        elif is_numeric_dtype(dtype):
            return 'NUM'
        elif is_string_dtype(dtype):
            return 'TEXT'
        else:
            raise TypeError(f"Unable convert {dtype} to SQL type")

    def to_column_definition(item):
        column_name, dtype = item
        sql_type = dtype_to_sql_type(dtype)
        return f'"{column_name}" sql_type'

    with open(json_file_path, 'r') as data_file:
        data = json.load(data_file)

    df = DataFrame.from_records(list(data.values())) \
        .assign(some=data.keys()) \
        .set_index('some')
    df.index = df.index.astype('string')
    df['issued'] = df['issued'].astype('string')
    df['authors'] = df['authors'].apply(lambda l: ", ".join(l)).astype('string')

    column_definitions = map(to_column_definition, df.dtypes.to_dict().items())

    conn.executescript(f'''
    DROP TABLE IF EXISTS "{table_name}";
    CREATE TABLE "{table_name}" (
        "{df.index.name}" {dtype_to_sql_type(df.index.dtype)} PRIMARY KEY
        {"".join(map(lambda d: ", " + d, column_definitions))}
    );
    ''')
    df.to_sql(table_name, conn, if_exists='append')


def download_missing_abstracts(conn: Connection):
    unknown_pmids = pd.read_sql(f"""
    SELECT pmid FROM somes
    EXCEPT
    SELECT pmid FROM articles
    """, conn)['pmid']

    # Optionally, split `pmids` into smaller batches if necessary
    batch_size = 1000  # Example batch size
    for i in range(0, len(unknown_pmids), batch_size):
        batch_pmids = unknown_pmids[i:i + batch_size]

        print(f"Fetching abstract for {len(batch_pmids)} PMIDs...")
        pmids_str = ",".join(map(str, batch_pmids))
        handle = Entrez.efetch(db=entrez_database, id=pmids_str, retmode=entrez_retmode, rettype="abstract")
        records = Entrez.read(handle)

        def extract_interesting_fields(article):
            interesting_fields = {
                'pmid': int(article['MedlineCitation']['PMID']),
            }

            try:
                interesting_fields['abstract_text'] = \
                    article['MedlineCitation']['Article']['Abstract']['AbstractText'][0]
            except KeyError:  # Abstract not found or other parsing issue
                interesting_fields['abstract_text'] = None

            return interesting_fields

        conn.executemany("INSERT INTO articles (pmid, abstract) VALUES (:pmid, :abstract_text)",
                         map(extract_interesting_fields, records['PubmedArticle']))


def calculate_contexts(template: str, conn: Connection):
    holes = []

    def replacer(match):
        nonlocal holes
        hole_id = len(holes)
        holes.append(match['field_name'])

        result = '{' + str(hole_id)

        if match['conversion'] is not None:
            result += '!' + match['conversion']

        if match['format_spec'] is not None:
            result += ':' + match['format_spec']

        result += '}'

        return result

    template = re.sub(
        r'\{(?P<field_name>[^!:}]*)(?:!(?P<conversion>[rsa]))?(?::(?P<format_spec>[^}]*))?}',
        replacer, template)

    columns = pd.read_sql('PRAGMA table_info(somes)', conn)
    if 'context' not in columns['name'].values:
        conn.executescript("""
        ALTER TABLE somes ADD COLUMN context TEXT;
        ALTER TABLE somes ADD COLUMN context_hash TEXT;
        """)
    conn.execute(f"""
    UPDATE somes
    SET context=substitute_template(?{''.join(map(lambda s: ', ' + s, holes))})
    FROM articles
    WHERE somes.pmid = articles.pmid
      AND ({' AND '.join(map(lambda s: f'({s}) IS NOT NULL', holes)) or 'TRUE'})
    """, (template,))
    conn.executescript("""
    UPDATE somes SET context_hash = hash_context(context);
    """)


def process_missing_contexts(conn: Connection):
    conn.executescript("""
    CREATE TEMP TABLE contexts_to_process AS
    SELECT context_hash, context
    FROM somes
    LEFT JOIN embeddings ON text_hash = context_hash
    WHERE embedding IS NULL AND context IS NOT NULL;
    """)

    try:
        # Estimate token usage and ask for user confirmation
        entry_count, total_tokens = conn.execute('''
        SELECT COUNT(*), SUM(token_count_of(context))
        FROM contexts_to_process
        ''').fetchone()

        if entry_count == 0:
            return

        print(f"{entry_count} embeddings needs to be calculated. Total tokens to process: {total_tokens}, estimated cost: ${total_tokens / 1000 * cost_of_1k_token}")
        confirmation = input("Proceed with the embedding calculation? (yes/no): ")
        if confirmation.lower() == 'yes':
            contexts_to_process = pd.read_sql("SELECT context_hash, context FROM contexts_to_process", conn)
            for chunk in np.array_split(contexts_to_process, math.ceil(len(contexts_to_process) / 2048)):
                print(f"Calculating {len(chunk)} embeddings...")
                response = openai.embeddings.create(input=chunk['context'], model=embedding_model)
                print(response.usage)
                embeddings = [item.embedding for item in response.data]

                conn.executemany("INSERT INTO embeddings (text_hash, embedding) VALUES (?, ?)",
                                 zip(chunk['context_hash'], map(np.array, embeddings)))
        else:
            print("Continuing without the new embeddings")

    finally:
        conn.executescript("DROP TABLE contexts_to_process;")


def calculate_missing_pca(conn: Connection):
    needs_to_recalculate = conn.execute("""
    SELECT COUNT(*) FROM (
        SELECT some FROM somes WHERE context_hash IN (SELECT text_hash FROM embeddings)
        EXCEPT
        SELECT some FROM pca WHERE template_hash=?
    )
    """, (hash_context(context_template),)).fetchone()[0]

    if needs_to_recalculate == 0:
        return
    else:
        print("Performing PCA...")

    df = pd.read_sql("""
    SELECT some, embedding
    FROM somes
    INNER JOIN embeddings ON embeddings.text_hash = somes.context_hash
    """, conn)
    df['embedding'] = df['embedding'].apply(np.frombuffer)

    # Calculate PCA
    n_components = 2
    pca = DIMENSION_REDUCTION(n_components=n_components)
    principal_components = pca.fit_transform(np.array(df['embedding'].to_list()))

    df = df.drop(['embedding'], axis='columns')
    df['template_hash'] = hash_context(context_template)
    for i in range(n_components):
        df[f'pc_{i}'] = principal_components[:, i]

    conn.executemany(f"""
    INSERT OR REPLACE INTO pca({', '.join(df.columns)})
    VALUES ({', '.join('?' * len(df.columns))})
    """, df.to_records(index=False))


def optimize_database():
    print("Optimizing database...", end='\r')
    shutil.copy(database_path, database_path + '~')
    conn = connect()
    try:
        try:
            conn.executescript("ALTER TABLE somes DROP COLUMN context;")
        except sqlite3.OperationalError as err:
            excepted_details = (1, 'SQLITE_ERROR', ('no such column: "context"',))
            if (err.sqlite_errorcode, err.sqlite_errorname, err.args) == excepted_details:
                pass
            else:
                raise
        conn.executescript(f"""
        DROP TABLE IF EXISTS articles;
        DELETE FROM pca WHERE template_hash<>'{hash_context(context_template)}' OR some NOT IN (SELECT some FROM somes);
        DELETE FROM embeddings WHERE text_hash NOT IN (SELECT context_hash FROM somes);
        VACUUM;
        """)
    finally:
        conn.close()
    print(f"Database size: {math.ceil(math.ceil(os.path.getsize(database_path) / 1024) / 1024)}M   ")


def update_db_content(run_optimization=False):
    conn = connect()
    try:
        print("Ensuring database structure...   ", end="\r")
        ensure_tables(conn)

        print("Loading dataset...               ", end="\r")
        load_somes(json_file_path, conn)

        print("Looking for missing abstracts... ", end="\r")
        download_missing_abstracts(conn)

        print("Checking for missing contexts... ", end="\r")
        calculate_contexts(context_template, conn)

        print("Looking for missing embeddings...", end="\r")
        process_missing_contexts(conn)

        print("Checking for PCA...              ", end="\r")
        calculate_missing_pca(conn)

        if run_optimization:
            print("Optimizing database...           ", end="\r")
            optimize_database(conn)

        print("Database up to date.             ")
    finally:
        conn.close()
