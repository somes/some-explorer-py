from configargparse import ArgumentParser

parser = ArgumentParser("some-explorer")
parser.add_argument('-d', '--debug', action='store_true', help="Run server in debug mode")
parser.add_argument('-u', '--update-db-content', action='store_true', help="Update DB content before start")
parser.add_argument('-o', '--optimize-db', action='store_true', help="Optimize database - make sure you have a backup")
parser.add_argument('-p', '--port', type=int, help="Port to use for running the webserver",
                    env_var='PORT', default=8050)
parser.add_argument('-N', '--no-server', action='store_true', help="Do not start server, apply side effects only")
args = parser.parse_args()
